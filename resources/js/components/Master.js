import React, { Component } from 'react';
import { Router, Route, Link } from 'react-router';
import MyGlobleSetting from './MyGlobleSetting';

export default class Master extends Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-3">
                    <div className="card">
                        <div className="card-header">Navigation</div>
                        <div className="card-body">                
                            <ul className="nav flex-column">
                                <li className="nav-item">
                                    <Link className="nav-link active" to="/users">Users</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to="/display-item">Display Users</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to="/add-user">Add User</Link>
                                </li>                                
                            </ul>
                        </div>
                    </div>
                    </div>
                    <div className="col-md-9">
                        {this.props.children}
                    </div>
                </div>
            </div>
        );
    }
}

// if (document.getElementById('example')) {
//     ReactDOM.render(<Example />, document.getElementById('example'));
// }
