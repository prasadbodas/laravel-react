import React, { Component } from 'react';
import axios from 'axios';
import { Link, browserHistory } from 'react-router';
import MyGlobleSetting from './MyGlobleSetting';

export default class CreateUser extends Component {
    constructor(props){
        super(props);
        this.state = {
            userName: '',
            userEmail: '', 
            userPassword: ''
        };
    
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }
      handleChangeName(e){
        this.setState({
          userName: e.target.value
        })
      }
      handleChangeEmail(e){
        this.setState({
          userEmail: e.target.value
        })
      }
      handleChangePassword(e){
        this.setState({
          userPassword: e.target.value
        })
      }
      handleSubmit(e){
        e.preventDefault();
        const user = {
          name: this.state.userName,
          email: this.state.userEmail,
          password: this.state.userPassword
        }
        let uri = MyGlobleSetting.url + 'users';
        axios.post(uri, user).then((response) => {
            browserHistory.push('/display-item');
        });
      }
      
    render() {
        return (
            <div>
                <h1>Create A User</h1>
                <div className="row">
                    <div className="col-md-10"></div>
                    <div className="col-md-2">
                        <Link to="/display-item" className="btn btn-success">Return to Users</Link>
                    </div>
                </div>
                <form onSubmit={this.handleSubmit}>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Name:</label>
                                <input type="text" className="form-control" onChange={this.handleChangeName} />
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                            <label>Email:</label>
                            <input type="email" className="form-control" onChange={this.handleChangeEmail} />
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                            <label>Password:</label>
                            <input type="text" className="form-control" onChange={this.handleChangePassword} />
                            </div>
                        </div>
                    </div>
                    <div className="form-group">
                        <button className="btn btn-primary">Add User</button>
                    </div>
                </form>
        </div>
        );
    }
}

// if (document.getElementById('sidebar')) {
//     ReactDOM.render(<Example />, document.getElementById('sidebar'));
// }
