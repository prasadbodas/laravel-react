import React, { Component } from 'react';
import axios from 'axios';
import { Link, browserHistory } from 'react-router';
import TableRow from './TableRow';
import MyGlobleSetting from './MyGlobleSetting';

export default class DisplayUser extends Component {
    constructor(props) {
        super(props);
         this.state = {value: '', users: ''};
       }
 
      componentDidMount(){
         axios.get(MyGlobleSetting.url + 'users')
         .then(response => {
           this.setState({ users: response.data });
         })
         .catch(function (error) {
           console.log(error);
         })
       }
       tabRow(){
         if(this.state.users instanceof Array){
           return this.state.users.map(function(object, i){
               return <TableRow obj={object} key={i} />;
           })
         }
       }

    render() {
        return (
            <div>
                <h1>Users</h1>
                <div className="row">
                    <div className="col-md-10"></div>
                    <div className="col-md-2">
                    <Link to="/add-user">Create User</Link>
                    </div>
                </div>
                <table className="table table-hover">
                    <thead>
                        <tr>
                            <td>ID</td>
                            <td>Name</td>
                            <td>Email</td>
                            <td width="200px">Actions</td>
                        </tr>
                    </thead>
                    <tbody>
                        {this.tabRow()}
                    </tbody>
                </table>
            </div>
        )
    }
}

// if (document.getElementById('sidebar')) {
//     ReactDOM.render(<Example />, document.getElementById('sidebar'));
// }
