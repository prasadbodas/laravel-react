import React, { Component } from 'react';
import axios from 'axios';
import { Link, browserHistory } from 'react-router';
import MyGlobleSetting from './MyGlobleSetting';

export default class UpdateUser extends Component {
    constructor(props) {
        super(props);
        this.state = {name: '', email: ''};
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    componentDidMount(){
      axios.get(MyGlobleSetting.url + `users/${this.props.params.id}/edit`)
      .then(response => {
        this.setState({ 
            name: response.data.name, 
            email: response.data.email
        });
      })
      .catch(function (error) {
        console.log(error);
      })
    }
    handleChangeName(e){
      this.setState({
        name: e.target.value
      })
    }
    handleChangeEmail(e){
      this.setState({
        email: e.target.value
      })
    }
  
    handleSubmit(event) {
      event.preventDefault();
      const products = {
        name: this.state.name,
        email: this.state.email
      }
      let uri = MyGlobleSetting.url + 'users/'+this.props.params.id;
      axios.patch(uri, products).then((response) => {
            browserHistory.push('/display-item');
      });
    }
    render() {
        return (
            <div>
                <h1>Update Item</h1>
                <div className="row">
                    <div className="col-md-10"></div>
                    <div className="col-md-2">
                        <Link to="/display-item" className="btn btn-success">Return to Users</Link>
                    </div>
                </div>
                <form onSubmit={this.handleSubmit}>
                <div className="row">
                    <div className="col-md-6">
                            <div className="form-group">
                                <label>Name:</label>
                                <input type="text" className="form-control" onChange={this.handleChangeName} value={this.state.name} />
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                            <label>Email:</label>
                            <input type="email" className="form-control" onChange={this.handleChangeEmail} value={this.state.email} />
                            </div>
                        </div>
                    </div>

                    <div className="form-group">
                        <button className="btn btn-primary">Update</button>
                    </div>
                </form>
            </div>
        );
    }
}

// if (document.getElementById('sidebar')) {
//     ReactDOM.render(<Example />, document.getElementById('sidebar'));
// }
